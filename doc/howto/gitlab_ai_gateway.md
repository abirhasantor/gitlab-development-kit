# GitLab AI Gateway

Configure the [GitLab AI Gateway](https://gitlab.com/gitlab-org/modelops/applied-ml/code-suggestions/ai-assist)
to run locally in GDK.

This installation method is a simpler alternative to
[manually cloning, installing, and running the AI Gateway locally](https://gitlab.com/gitlab-org/modelops/applied-ml/code-suggestions/ai-assist#how-to-run-the-server-locally).

To configure:

1. Run `gdk config set gitlab_ai_gateway.enabled true`.
1. After you have received [access to Anthropic and Google Cloud](https://docs.gitlab.com/ee/development/ai_features/#access),
   add the following to the `env.runit` file in the root of your GDK directory:

   ```shell
    export ANTHROPIC_API_KEY=$YOUR_API_KEY
    export AIGW_VERTEX_SEARCH__PROJECT="ai-enablement-dev-69497ba7"
   ```

1. Run `gdk update`.
1. Run `gdk start gitlab-ai-gateway`.
1. You can watch the status of the service by running `gdk tail gitlab-ai-gateway`
1. Go to the [GitLab AI Gateway API documentation page](http://localhost:5052/docs)
   to verify that the GitLab AI Gateway started.

For more information, see the
[AI features based on 3rd-party integrations](https://docs.gitlab.com/ee/development/ai_features/index.html)
page.
