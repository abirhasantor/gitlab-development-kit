# frozen_string_literal: true

module GDK
  module Command
    class Telemetry < BaseCommand
      def run(_ = [])
        puts GDK::Telemetry::PROMPT_TEXT

        username = $stdin.gets&.chomp
        GDK::Telemetry.update_settings(username)

        puts \
          case username
          when '.'
            'Error tracking and analytic data will not be collected.'
          when '', NilClass
            'Error tracking and analytic data will now be collected anonymously.'
          else
            "Error tracking and analytic data will now be collected as '#{username}'."
          end

        true
      end
    end
  end
end
